from flask import Flask,render_template,redirect,request,url_for
from config import SQLALCHEMY_DATABASE_URI
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import login_user, LoginManager, UserMixin, login_required, logout_user, current_user
from werkzeug.security import check_password_hash
from datetime import datetime


app = Flask(__name__)
app.config["DEBUG"] = True
app.config["DEBUG"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# secret key for encryption
app.secret_key = "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin."
login_manager = LoginManager()
login_manager.init_app(app)

db=SQLAlchemy(app)
migrate=Migrate(app,db)

# define user class
class User(UserMixin,db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128))
    password_hash = db.Column(db.String(128))

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


    def get_id(self):
        return self.username

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(username=user_id).first()

# define comments table
class Comment(db.Model):

    __tablename__ = "comments"

    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(5000))
    posted = db.Column(db.DateTime, default=datetime.now)
    commenter_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    commenter = db.relationship('User', foreign_keys=commenter_id)

@app.route('/',methods=['GET','POST'])
def home():
    if request.method=='GET':
        return render_template("main_page.html",comments=Comment.query.all())

    # post method to add new comment

    # validate user
    if not current_user.is_authenticated:
        return redirect(url_for('home'))

    # create Comment object
    comment = Comment(content=request.form["contents"],commenter=current_user)
    # commit the new comment in the ORM session
    db.session.add(comment)
    db.session.commit()
    db.session.close()
    return redirect(url_for('home'))

@app.route('/login/',methods=['GET','POST'])
def login():
    if request.method=='GET':
        return render_template("login_page.html",error=False)

    # check for valid users
    user = load_user(request.form["username"])
    if user is None:
        return render_template("login_page.html",error=True)

    # validate password
    if not user.check_password(request.form["password"]):
        return render_template("login_page.html",error=True)

    login_user(user)

    # redirect to login page on login success
    return redirect(url_for('home'))

@app.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
